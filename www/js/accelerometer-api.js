var watchID = null;

// Wait for device API libraries to load
    //
    //document.addEventListener("deviceready", onDeviceReady, false);

    // device APIs are available
    //
    function getAccelerometerRunning() {
        var options = { frequency: 500 };

        watchID = navigator.accelerometer.watchAcceleration(onAccelerometerSuccess, onAccelerometerError, options);
    }


    // onSuccess: Get a snapshot of the current acceleration
    //
    function onAccelerometerSuccess(acceleration) {
        var element = document.getElementById('accelerometer');
        element.innerHTML = 'Acceleration X: ' + acceleration.x         + '<br />' +
                            'Acceleration Y: ' + acceleration.y         + '<br />' +
                            'Acceleration Z: ' + acceleration.z         + '<br />' +
                            'Timestamp: '      + acceleration.timestamp + '<br />';
    
    }

    // onError: Failed to get the acceleration
    //
    function onAccelerometerError() {
        alert('onError!');
    }

        // Stop watching the acceleration
    //
    function stopAccelerometerRunning() {
        if (watchID) {
            navigator.accelerometer.clearWatch(watchID);
            watchID = null;
            //change html display
            var element = document.getElementById('accelerometer');
            element.innerHTML = 'Waiting for accelerometer ...'
        }
    }
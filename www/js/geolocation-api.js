// Wait for device API libraries to load
    //
    //document.addEventListener("deviceready", onDeviceReady, false);

    // device APIs are available
    //

    function getLocation() {
        navigator.geolocation.getCurrentPosition(onGeolocationSuccess, onGeolocationError);
    }

    // onSuccess Geolocation
    //
    function onGeolocationSuccess(position) {
        var element = document.getElementById('geolocation');
        element.innerHTML = 'Latitude: '           + position.coords.latitude              + '<br />' +
                            'Longitude: '          + position.coords.longitude             + '<br />' +
                            'Altitude: '           + position.coords.altitude              + '<br />' +
                            'Accuracy: '           + position.coords.accuracy              + '<br />' +
                            'Altitude Accuracy: '  + position.coords.altitudeAccuracy      + '<br />' +
                            'Heading: '            + position.coords.heading               + '<br />' +
                            'Speed: '              + position.coords.speed                 + '<br />' +
                            'Timestamp: '          + position.timestamp                    + '<br />';

        //MAP
        var myLat = position.coords.latitude;
        var myLong = position.coords.longitude;
        var myLatlng = new google.maps.LatLng(myLat,myLong);
        var mapOptions = {
        center: myLatlng,
        zoom: 14,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    var map = new google.maps.Map(document.getElementById("map_canvas"),
                                  mapOptions);
    google.maps.event.trigger(map, 'resize');
    var marker = new google.maps.Marker({
    position: myLatlng,
    animation: google.maps.Animation.BOUNCE
});

// To add the marker to the map, call setMap();
marker.setMap(map);
    }

    // onError Callback receives a PositionError object
    //
    function onGeolocationError(error) {
        alert('code: '    + error.code    + '\n' +
              'message: ' + error.message + '\n');
    }

    function clearLocation(){
        var element = document.getElementById('geolocation');
        element.innerHTML = 'Waiting for location update ...';
        var myLatlng = new google.maps.LatLng(37.421999, -122.083954);
        var mapOptions = {
        center: myLatlng,
        zoom: 14,
        mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        var map = new google.maps.Map(document.getElementById("map_canvas"),
                                  mapOptions);



    }

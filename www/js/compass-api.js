
var watchID = null;
function startCompass(){

	var options = {
    frequency: 500
	}; // Update every 0.5 seconds

	watchID = navigator.compass.watchHeading(compassSuccess, compassError, options);
}

function compassSuccess(heading){

	var element = document.getElementById("compass");

	element.innerHTML = 'Heading: ' + heading.magneticHeading;
}


function compassError(compassError) {
        alert('Compass Error: ' + compassError.code);
    }

  // Stop watching the compass
    //
    function stopCompass() {
        if (watchID) {
            navigator.compass.clearWatch(watchID);
            watchID = null;
            var element = document.getElementById("compass");

			element.innerHTML = 'Waiting for compass reading ...';
        }
    }

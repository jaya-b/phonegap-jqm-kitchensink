
        var myMedia = null;
        var playing = false;
  
        function playAudio() {
          if (!playing) {
            myMedia.play(); 
            document.getElementById('play').src = "images/pause.png";
            playing = true; 
          } else {
              myMedia.pause();
              document.getElementById('play').src = "images/play.png";    
              playing = false; 
          }
        }//End of playAudio() function
 
        function stopAudio() {
          myMedia.stop();
          playing = false;
          document.getElementById('play').src = "images/play.png";    
          document.getElementById('audio_position').innerHTML = "0.000 sec";
          $( "#progressbar" ).progressbar({value: 0}) ;
        }//End of stopAudio() function
 
        function onLoad() {
          document.addEventListener("deviceready", onDeviceReady, false);
        }
   
        function onDeviceReady(){
          console.log("Got device ready");
          updateMedia();
        }
   
        function updateMedia(src) {
          // Clean up old file
          if (myMedia != null) {
            myMedia.release();
          }
    
          // Get the new media file
          var yourSelect = document.getElementById('playlist');   
          myMedia = new Media(yourSelect.options[yourSelect.selectedIndex].value, stopAudio, null);
 
          // Update media position every second
          var mediaTimer = setInterval(function() {
              // get media position
              myMedia.getCurrentPosition(
                // success callback
                function(position) {
                  if (position >= 0) {
                    document.getElementById('audio_position').innerHTML = (position) + " sec";
                    var dur = myMedia.getDuration();
                    $( "#progressbar" ).progressbar({value: (position/dur)*100});
                  } 
                },
                // error callback
                function(e) {
                  console.log("Error getting pos=" + e);
                }
              );
          }, 1000);
        }//End of updateMedia() function
 
        function setAudioPosition(position) {
          document.getElementById('audio_position').innerHTML =position;
        }